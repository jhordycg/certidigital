import AddIcon from "@mui/icons-material/Add";
import { LoadingButton } from "@mui/lab";
import { Box, Grid, List, ListItem, ListItemText } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import NCollectionType from "./types/NCollectionType";
import { useUser } from "./user-context";

const fetchRequests = async (url: string) => axios.get(url).then(resp => resp.data);
const createRequest = async (data: any) => {
    const resp = await axios.post("api/requests", data);
    return resp.data;
}

export default function Requests() {
    // Prepare Page
    const { user } = useUser();
    const router = useRouter();
    //useEffect(() => { if (!user) router.push("/"); }, [user]);
    // End Prepare

    const [collections, setCollections] = useState<NCollectionType[]>([]);
    const [sendingRequest, setSendingRequest] = useState(false);

    const addCollection = () => {
        const date = new Date();
        const codigoCollection = `20120099092%ADS%${user.dni}`
        const newCollection: NCollectionType = {
            "entity-type": "document",
            type: "Collection",
            properties: {
                "dc:title": `${user.institute} - Expediente ${user.dni}`,
            }
        }
        setCollections(collections.concat([newCollection]));
        console.log(codigoCollection);
    }

    return (<>
        <Box textAlign="center">
            <h2>Solicitud de Expedientes</h2>
        </Box>
        <Grid container  >
            <Grid item xs={5} paddingX={2}>
                <List>
                    {collections.map((item, key) => (
                        <ListItem key={key}>
                            <ListItemText>{item.properties["dc:title"]}</ListItemText>
                        </ListItem>
                    ))}
                </List>
                <LoadingButton onClick={() => addCollection()}
                    variant="contained" color="primary" size="large" endIcon={<AddIcon />}
                >
                    Crear Nuva Solicitud
                </LoadingButton>
            </Grid>
        </Grid>
    </>
    );
}