import { Alert, AlertColor, Avatar, IconButton, ListItemAvatar, ListItemButton, ListItemSecondaryAction, ListItemText, Skeleton, Snackbar, Stack } from "@mui/material";
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import { useState } from 'react';
import Grid from '@mui/material/Grid';
import { FilePresent, FileUpload } from "@mui/icons-material";
import { styled } from '@mui/material/styles';
import { LoadingButton } from "@mui/lab";
import axios from "axios";
import { useUser } from "./user-context";


type RequirementItemProps = {
    id: number,
    requirement: string,
    onChangeFile: Function,
    onClick: Function,
}

const Input = styled('input')({
    display: 'none',
});

function RequirementPage() {
    const { user } = useUser();

    const [showAlert, setShowAlert] = useState<AlertColor>();
    const [alertMessage, setAlertMessage] = useState<string>();

    const [preview, setPreview] = useState<string>();
    const [files, setFiles] = useState<Map<string, File>>(new Map());


    const requirements: string[] = [
        "Documento de Identidad",
        "Partida de Nacimiento",
        "Foto Tamaño Pasaporte",
    ]

    const sendRequirements = async () => {
        console.log("Step 1")
        if (files.size == requirements.length) {
            console.log("Step 1")
            files.forEach(async (item, key) => {
                console.log("Step 1")
                // Add user data.
                const form = new FormData();
                //"default-domain/UserWorkspaces/74859674"
                console.log(`default-domain/UserWorkspaces/${user.dni}`);
                form.append("path", `default-domain/UserWorkspaces/${user.dni}`);
                form.append("username", user.dni);
                form.append("password", user.password)
                form.append("type", key);
                form.append("requirement", item);
                const resp = await axios.post("/api/document-handle", form);
                console.log(resp);
            });
        } else {
            setShowAlert("error");
            setAlertMessage("porfavor agregue todos sus archivos");
            console.log("porfavor agregue todos sus archivos");
        }
    }

    const setFile = (key: string, value: File) => {
        setFiles(files.set(key, value));
        setPreview(URL.createObjectURL(value));
    }

    return (<>
        <Snackbar open={showAlert != undefined}
            anchorOrigin={{ horizontal: "right", vertical: "top" }}
            autoHideDuration={6000}
        >
            <Alert
                severity={showAlert}
                sx={{ width: '100%' }}>
                {alertMessage}
            </Alert>
        </Snackbar>
        {
            true ?
                <Grid container padding={1} columnSpacing={1} height="100vh" alignItems="stretch" justifyContent="space-around">
                    <Grid item xs={6}>
                        <List>
                            {requirements.map((item: string, i: number) => {
                                return (<RequirementItem key={i} id={i} requirement={item} onChangeFile={setFile} onClick={setPreview} />);
                            })}
                        </List>
                        <LoadingButton variant="contained" onClick={() => sendRequirements()} >Enviar</LoadingButton>
                    </Grid>
                    <Grid item xs={6} >
                        {preview ?
                            <iframe src={preview} width="100%" height="100%" />
                            :
                            <Skeleton variant="rectangular" height="100%" />
                        }
                    </Grid>
                </Grid>
                : <RequirementSkeleton />
        }
    </>
    );
}



function RequirementItem({ id, requirement, onChangeFile, onClick }: RequirementItemProps) {
    const [file, setFile] = useState<File>();

    const handleFile = (files: FileList | null) => {
        if (files && files.item(0) !== null) {
            var selectedFile = files.item(0);
            if (selectedFile !== null && selectedFile.type.startsWith("image/")) {
                setFile(selectedFile);
                onChangeFile(requirement, selectedFile);
            }
        }
    }

    const handleOnClick = () => {
        console.log("config preview")
        if (file) {
            onClick(URL.createObjectURL(file));
        }
    }

    return (
        <ListItem disablePadding >
            <ListItemButton onClick={() => handleOnClick()}>
                <ListItemAvatar>
                    <Avatar>
                        <FilePresent />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={requirement} secondary={file?.name} />
            </ListItemButton>
            <ListItemSecondaryAction>
                <label htmlFor={`icon-button-file-${id}`} >
                    <Input accept="image/*" id={`icon-button-file-${id}`} type="file"
                        multiple={false} onChange={({ target }) => handleFile(target.files)}
                    />
                    <IconButton color="primary" aria-label="upload file" component="span" >
                        <FileUpload />
                    </IconButton>
                </label>
            </ListItemSecondaryAction>
        </ListItem>
    );
}

function RequirementSkeleton() {
    const requirements = new Array();
    return (
        <Grid container padding={1} columnSpacing={1} height="100vh" alignItems="stretch" justifyContent="space-around">
            <Grid item xs={6}>
                <List>
                    <ListItem>
                        <ListItemAvatar>
                            <Skeleton variant="circular">
                                <Avatar />
                            </Skeleton>
                        </ListItemAvatar>
                        <Stack width="60%">
                            <Skeleton width="50%" height={24} />
                            <Skeleton />
                        </Stack>
                        <ListItemSecondaryAction>
                            <IconButton  >
                                <Skeleton variant="circular" height={24} width={24} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Skeleton variant="circular">
                                <Avatar />
                            </Skeleton>
                        </ListItemAvatar>
                        <Stack width="60%">
                            <Skeleton width="50%" height={24} />
                            <Skeleton />
                        </Stack>
                        <ListItemSecondaryAction>
                            <IconButton  >
                                <Skeleton variant="circular" height={24} width={24} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Skeleton variant="circular">
                                <Avatar />
                            </Skeleton>
                        </ListItemAvatar>
                        <Stack width="60%">
                            <Skeleton width="50%" height={24} />
                            <Skeleton />
                        </Stack>
                        <ListItemSecondaryAction>
                            <IconButton  >
                                <Skeleton variant="circular" height={24} width={24} />
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Grid>
            <Grid item xs={6} >
                <Skeleton variant="rectangular" height="100%" />
            </Grid>
        </Grid>
    );
}

export default RequirementPage;