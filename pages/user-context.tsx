import { createContext, useContext, useMemo, useState } from "react";
import UserType from "./types/UserType";

const UserContext = createContext<{ user: UserType, login: Function } | undefined>(undefined);

export function UserProvider(props: any) {
    const [user, setUser] = useState<UserType>();

    const login = (user: UserType) => {
        setUser(user);
    };

    const savedUser = useMemo(() => {
        return { user, login };
    }, [user]);

    return <UserContext.Provider value={savedUser} {...props} />;
}

export function useUser() {
    const context = useContext(UserContext);
    if (!context) throw new Error("Usuario no Existe, pendejo de axila");
    return context;
}