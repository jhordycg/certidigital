import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next'
import UserType from '../types/UserType';

const FILE_SERVER = process.env.FILE_SERVICE;

export default async function handler(
    request: NextApiRequest,
    response: NextApiResponse
) {
    if (request.method == "GET") await getBackUp(request, response);
    else if (request.method == "POST") await postHandle(request, response);
    else response.status(405).json({ message: "Method Not Allowed" });
}

async function getBackUp(request: NextApiRequest, response: NextApiResponse) {
    response.status(200).json([{
        area: "Administrativa",
        careers: "Analisis de sistemas",
        dni: "77077399",
        email: "julio.quispe@vallegrande.edu.pe",
        id: "61bbfba9a945097ef19da6cb",
        institute: "Valle Grande",
        lastname: "Quispe Chavez",
        name: "Julio Cesar",
        password: "ssasass123",
        phone: "987778961",
        rol: "ADMINISTRATOR"
    }, {
        area: "Administrativa",
        careers: "Analisis de sistemas",
        dni: "77077400",
        email: "victor.sandoval@vallegrande.edu.pe",
        id: "61bbfc78b5c20a0c7c1d464c",
        institute: "Valle Grande",
        lastname: "Sandoval Sanchez",
        name: "Victor",
        password: "vg123",
        phone: "912342237",
        rol: "STUDENT"
    }, {
        area: "Academica",
        careers: "Analisis de sistemas",
        dni: "77077401",
        email: "luis.apumac@vallegrande.edu.pe",
        id: "61bbfcbdb5c20a0c7c1d464d",
        institute: "Valle Grande",
        lastname: "Apumac Lopez",
        name: "Luis Albert",
        password: "vgluis1245",
        phone: "912342234",
        rol: "COLABORATOR"
    }, {
        area: "Academica",
        careers: "Administracion",
        dni: "74859674",
        email: "julisa.huapa@gmail.com",
        id: "61bc939382836d2427c07d78",
        institute: "Condoray",
        lastname: "HUAPAYA",
        name: "JULISSA",
        password: "Y/SJGx6gzam/",
        phone: "951456852",
        rol: "COLABORATOR"
    }, {
        area: "Academica",
        careers: "Analisis de sistemas",
        dni: "72036321",
        email: "diego-hr2010@vallegrande.edu.pe",
        id: "61bcbb1e993daa046f6f3175",
        institute: "Valle Grande",
        lastname: "Huapaya Rivera",
        name: "Diego",
        password: "contraseña",
        phone: "955140727",
        rol: "STUDENT"
    }]);
}

async function getHandle(
    request: NextApiRequest,
    response: NextApiResponse
) {
    const url = "http://40.91.114.117:8080/repository/user";
    try {
        const resp = await axios.get(url);
        let sortedData = resp.data;
        if (sortedData instanceof Array) {
            sortedData = resp.data.sort((a: any, b: any) => (a["rol"] as string).localeCompare(b["rol"]));
        }
        response.status(200).json(sortedData);
    } catch (err) {
        console.log(err);
        throw new Error("error");
    }
}

async function postHandle(
    request: NextApiRequest,
    response: NextApiResponse
) {
    const axiosConfig: AxiosRequestConfig = {
        auth: {
            username: "Administrator",
            password: "Administrator",
        }
    };

    try {
        const data: UserType = request.body;

        const dataToSend = {
            "entity-type": "user",
            id: data.id,
            properties: {
                firstName: data.name,
                lastName: data.lastname,
                username: data.dni,
                email: data.email,
                company: data.institute,
                password: data.password,
                groups: [(data.rol.toLowerCase())],
            },
            isAdministrator: false,
            isAnonimous: false
        };

        const url = `${FILE_SERVER}/nuxeo/api/v1/user`;
        const resp = await axios.post(url, dataToSend, axiosConfig);

        console.log(resp)
        if (resp.status == 409) {
            console.log("409")
            response.status(409).json(resp.data);
        }
        response.status(200).json(resp.data);
    } catch (err: any) {
        console.log(err)
        const error = err as AxiosError;
        if (error.request.res.statusCode == "409") response.status(200).json({ message: error.message });
        else throw error;
    }
}

