import axios, { AxiosRequestConfig } from "axios";
import FormData from "form-data";
import type { NextApiRequest, NextApiResponse } from "next";

const NUXEO_HOST = "http://54.87.204.159:32016/nuxeo/api/v1";

const config: AxiosRequestConfig = {
    baseURL: NUXEO_HOST,
};

//Data Type
type DocumentQuery = {
    "entity-type": "document",
    type: "File",
    uid? : string,
    title? : string,
    properties: {
        "dc:title"?: String
    }
}


//Request
export default async function documentsearch(
    request: NextApiRequest,
    response: NextApiResponse,
): Promise<void> {
    console.log(request);
    response.status(200).json(undefined);
}