import axios, { AxiosRequestConfig } from "axios";
import FormData from "form-data";
import formidable from "formidable";
import { readFile } from "fs/promises";
import type { NextApiRequest, NextApiResponse } from "next";

const NUXEO_HOST = `${process.env.FILE_SERVICE}/nuxeo/api/v1`;

const axiosConfig: AxiosRequestConfig = {
    baseURL: NUXEO_HOST,
    /* auth: {
        username: "Administrator",
        password: "Administrator",
    } */
};

// Data Types
type BatchResponse = {
    batchId: string;
    fileIdx?: number,
    uploadType?: string,
    uploadedSize?: number,
};

type DocumentFile = {
    "entity-type": "document",
    type: "File",
    uid?: string,
    name: string,
    title?: string,
    properties: {
        "dc:title"?: string,
        "file:content"?: {
            "upload-batch"?: string,
            "upload-fileId"?: number,
        }
    }
}

// Request

export default async function documentHandle(
    request: NextApiRequest,
    response: NextApiResponse,
) {
    const forms = new formidable.IncomingForm({ keepExtensions: true });
    try {
        var formulary = await new Promise<
            { formData: FormData, authorization: string, title: string, path: string }
        >((resolve, reject) => {
            forms.parse(request, async (err, fields, files: formidable.Files) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    return;
                } else {
                    var formData = new FormData();
                    // Getting files
                    Object.keys(files).forEach(async (key) => {
                        var file = files[key];
                        if (!(file instanceof Array)) {
                            var fileReader = await readFile(file.filepath);
                            formData.append(key, fileReader, `${file.originalFilename}`);
                        }
                    });
                    let credentials = Buffer.from(`${fields["username"]}:${fields["password"]}`, "utf-8");;
                    let title = "";
                    // Getting fields

                    Object.keys(fields).forEach(async (key) => {
                        var field = fields[key];
                        if (!(field instanceof Array))
                            if (key == "type") title = field;
                    });
                    const path = `${fields["path"]}`;
                    const authorization = `Basic ${credentials.toString("base64")}`;
                    resolve({ formData, authorization, title, path });
                }
            });
        });

        const batchId = (await initializeBatch(formulary.authorization)).batchId;
        const fileIndex = (await uploadToBatch(batchId, formulary.formData, formulary.authorization)).fileIdx;

        const dataFile: DocumentFile = {
            "entity-type": "document",
            type: "File",
            name: formulary.title,
            properties: {
                "dc:title": formulary.title,
                "file:content": {
                    "upload-batch": batchId,
                    "upload-fileId": fileIndex ?? 0,
                }
            }
        };
        const resp = await createFile(dataFile, formulary.path, formulary.authorization);
        console.log(batchId);
        console.log(resp.uid);
        response.status(200).json({ message: resp.uid });
    } catch (err) {
        console.error("error: %s", err);
        response.status(500).json({ error: err });
    }
}

async function initializeBatch(
    basicAuth: string,
): Promise<BatchResponse> {
    try {
        const resp = await axios.post("upload/new/default", undefined, { ...axiosConfig, headers: { Authorization: basicAuth } });
        return resp.data;
    }
    catch (err) {
        throw new Error("error");
    }
}

async function uploadToBatch(
    batchId: string,
    data: FormData,
    basicAuth: string,
): Promise<BatchResponse> {
    try {
        const resp = await axios.post(`upload/${batchId}/0`, data,
            { headers: { ...data.getHeaders(), Authorization: basicAuth }, ...axiosConfig }
        );
        const dataFix = `${resp.data}`.replaceAll("</html>", "").replaceAll("<html>", "");
        console.log("Upload To Batch Result: %s", dataFix);
        return JSON.parse(dataFix);
    } catch (err) {
        throw new Error("error")
    }
}

async function createFile(
    data: DocumentFile,
    path: string,
    basicAuth: string,
): Promise<DocumentFile> {
    try {
        console.log("path/%s", path);
        const resp = await axios.post(`path/${path}`, data, { ...axiosConfig, headers: { Authorization: basicAuth } });
        return resp.data;
    } catch (err: any) {
        throw new err;
    }
}

export const config = {
    api: {
        bodyParser: false
    }
};