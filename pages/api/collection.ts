import axios, { AxiosRequestConfig } from "axios";
import { NextApiRequest, NextApiResponse } from "next";

const HOST = `${process.env.FILE_SERVICE}/nuxeo`;

async function handleRequest(
    request: NextApiRequest,
    response: NextApiResponse,
) {
    if (request.method == "POST") {
        console.log(request.query);
        if (request.query.docId) await addToCollection(request, response);
        else await createCollection(request, response);
    } else {
        response.status(403).json("method not allowed");
    }
}

async function createCollection(
    request: NextApiRequest,
    response: NextApiResponse,
) {
    console.log(request.query);
    const config: AxiosRequestConfig = {
        headers: {
            Authorization: `${request.headers.authorization}`
        }
    }

    try {
        const resp = await axios.post(`${HOST}/api/v1/automation/Collection.Create`, request.body, config);
        console.log(resp.data);
        response.status(resp.status).json(resp.data);
    } catch (err) {
        console.error(err);
    }
}

async function addToCollection(
    request: NextApiRequest,
    response: NextApiResponse,
) {
    const config: AxiosRequestConfig = {
        headers: {
            Authorization: `${request.headers.authorization}`
        }
    }

    try {
        const resp = await axios.post(`${HOST}/api/v1/id/${request.query?.docId}/@op/Document.AddToCollection`, request.body, config);
        console.log(resp.data);
        response.status(resp.status).json(resp.data);
    } catch (err) {
        console.error(err);
    }
}

export default handleRequest; 