import axios, { AxiosRequestConfig } from "axios";
import type { NextApiRequest, NextApiResponse } from "next";

const NUXEO_HOST = `${process.env.FILE_HOST}/nuxeo/api/v1`;

const axiosConfig: AxiosRequestConfig = {
    baseURL: NUXEO_HOST,
    auth: {
        username: "Administrator",
        password: "Administrator"
    }
};


//data types 

type Collection = {
    "entity-type": "docType",
    type: "Collection",
    uid?: string
}

export default async function handler(
    request: NextApiRequest,
    response: NextApiResponse
) {
    if (request.method == "GET") await getHandle(request, response)
}

async function getHandle(
    request: NextApiRequest,
    response: NextApiResponse
) {
    const resp = await axios.get(NUXEO_HOST);
    let sortedData = resp.data;
    response.status(200).json(sortedData)
    console.log(getHandle)
}

async function postHandle(
    request: NextApiRequest,
    response: NextApiResponse
) {

}
