import * as React from 'react';
import { IconButton, ListItemAvatar, ListItemSecondaryAction, ListItemText, ListSubheader, Skeleton, Stack } from "@mui/material";
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import Grid from '@mui/material/Grid';
import { Cancel, CheckCircle} from '@mui/icons-material';

function checklist() {
  return (
    <Grid container padding={1} columnSpacing={1} height="100vh" alignItems="stretch" justifyContent="space-around">
      <Grid item xs={6}>
        <List>
          <ListSubheader>Documents</ListSubheader>
          <ListItem>
            <ListItemText primary="Documento de Identidad - DNI" />
            <ListItemSecondaryAction>
              <IconButton size="large" color="success">
                <CheckCircle fontSize="inherit" />
              </IconButton>
              <IconButton size="large" color="error">
                <Cancel fontSize="inherit" />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem>
            <ListItemText primary="Foto Digital" />
            <ListItemSecondaryAction>
              <IconButton size="large" color="success">
                <CheckCircle fontSize="inherit" />
              </IconButton>
              <IconButton size="large" color="error">
                <Cancel fontSize="inherit" />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <ListItem>
            <ListItemText primary="Partida de Nacimiento" />
            <ListItemSecondaryAction>
              <IconButton size="large" color="success">
                <CheckCircle fontSize="inherit" />
              </IconButton>
              <IconButton size="large" color="error">
                <Cancel fontSize="inherit" />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        </List>

      </Grid>
      <Grid item xs={6} >
        <Skeleton variant="rectangular" height="100%" />
      </Grid>
    </Grid>
  );
}

export default checklist