import { Button, Grid, Stack } from "@mui/material";
import axios, { AxiosRequestConfig } from "axios";
import { MutableRefObject } from "react";
import AutoCollectionType from "../../types/requests/AutoCollecction";
import { useUser } from "../../user-context";

function Overview({ onNext, onPrev, batchId }: Properties) {
    const { user } = useUser();

    const createCollection = async () => {
        const config: AxiosRequestConfig = {
            auth: {
                username: user.dni,
                password: user.password,
            }
        }
        const collection: AutoCollectionType = {
            params: {
                name: `Expedient ${user.dni} - ${user.institute}`,
            }
        }

        try {
            const resp = await axios.post("api/collection", collection, config,);
            console.log(resp);
            batchId.current = `${resp.data?.uid}`;
            onNext();
        } catch (err) {
            console.error(err);
        }
    }

    return (
        <Grid item container direction="column" height="100%">
            <Grid item xs={11}>

            </Grid>
            <Grid item xs>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                    <Button size="medium" variant="contained" onClick={() => onPrev()}  >Regresar</Button>
                    <Button size="medium" onClick={() => createCollection()}  >Estoy de Acuerdo</Button>
                </Stack>
            </Grid>
        </Grid>
    );
}

type Properties = {
    onNext: Function,
    onPrev: Function,
    batchId: MutableRefObject<string | undefined>,
}

export default Overview;