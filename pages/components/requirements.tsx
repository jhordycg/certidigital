import { Avatar, Button, IconButton, ListItemAvatar, ListItemButton, ListItemSecondaryAction, ListItemText, Stack } from "@mui/material";
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import { MutableRefObject, useRef, useState } from 'react';
import Grid from '@mui/material/Grid';
import { FilePresent, FileUpload } from "@mui/icons-material";
import { styled } from '@mui/material/styles';
import { LoadingButton } from "@mui/lab";
import axios, { AxiosRequestConfig } from "axios";
import { useUser } from "../user-context";
import AddToCollectionType from "../types/requests/AutoAddToCollection";

type Properties = {
    onNext: Function,
    onPrev: Function,
    batchId: MutableRefObject<string | undefined>,
    attachRequirement: Function,
}

function Requirements({ onNext, onPrev, batchId }: Properties) {
    const { user } = useUser();
    const requirements = useRef<string[]>([]);

    const addRequirement = (fileId: string) => {
        console.log(fileId);
        requirements.current = requirements.current.concat(fileId);
        console.log("ahora hay %i archivos", requirements.current.length);
    }

    const addToCollection: AddToCollectionType = {
        params: {
            collection: `${batchId.current}`,
        }
    }

    const handleNext = () => {
        console.log("se subiran %n archivos", requirements.current.length)
        try {
            requirements.current.forEach(async (docId) => {
                let config: AxiosRequestConfig = {
                    params: {
                        docId: `${docId}`
                    },
                    auth: {
                        username: user.dni,
                        password: user.password,
                    }
                }
                const resp = await axios.post("api/collection", addToCollection, config);
                console.log(resp.data);
            });
            onNext();
        } catch (err) {
            console.log(err);
        }
    }


    return (
        <Grid item container direction="column" height="100%">
            <Grid item xs={11}>
                <RequirementPage attachRequirement={addRequirement} />
            </Grid>
            <Grid item xs>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                    <Button size="medium" variant="contained" onClick={() => onPrev()} >regresar</Button>
                    <Button size="medium" variant="outlined" onClick={() => handleNext()} >adjuntar documentos</Button>
                </Stack>
            </Grid>
        </Grid>
    );
}

type RequirementItemProps = {
    id: number,
    requirement: string,
    onChangeFile: Function,
    onClick: Function,
}

const Input = styled('input')({
    display: 'none',
});

function RequirementPage({ attachRequirement }: any) {
    const { user } = useUser();

    const [preview, setPreview] = useState<string>();
    const [files, setFiles] = useState<Map<string, File>>(new Map());

    const requirements: string[] = [
        "Documento de Identidad",
        "Partida de Nacimiento",
        "Foto Tamaño Pasaporte",
    ]

    const sendRequirements = async () => {
        if (files.size == requirements.length) {
            files.forEach(async (item, key) => {
                // Add user data.
                const form = new FormData();
                form.append("path", `default-domain/UserWorkspaces/${user.dni}`);
                form.append("username", user.dni);
                form.append("password", user.password)
                form.append("type", key);
                form.append("requirement", item);
                const resp = await axios.post("/api/document-handle", form);
                attachRequirement(resp.data.message);
            });
        } else {
        }
    }

    const setFile = (key: string, value: File) => {
        setFiles(files.set(key, value));
        setPreview(URL.createObjectURL(value));
    }

    return (<>
        <Grid container padding={1} columnSpacing={1} height={1} alignItems="stretch" justifyContent="space-around">
            <Grid item xs={6}>
                <List>
                    {requirements.map((item: string, i: number) => {
                        return (<RequirementItem key={i} id={i} requirement={item} onChangeFile={setFile} onClick={setPreview} />);
                    })}
                </List>
                <LoadingButton variant="contained" onClick={() => sendRequirements()}
                    disabled={requirements.length != files.size} >
                    subir requisitos</LoadingButton>
            </Grid>
            <Grid item xs={6} >
                <iframe src={preview} width="100%" height="100%" />
            </Grid>
        </Grid>
    </>
    );
}

function RequirementItem({ id, requirement, onChangeFile, onClick }: RequirementItemProps) {
    const [file, setFile] = useState<File>();

    const handleFile = (files: FileList | null) => {
        if (files && files.item(0) !== null) {
            var selectedFile = files.item(0);
            if (selectedFile !== null && (selectedFile.type.startsWith("image/") || selectedFile.type.startsWith("application/pdf"))) {
                setFile(selectedFile);
                onChangeFile(requirement, selectedFile);
            }
        }
    }

    const handleOnClick = () => {
        if (file) {
            onClick(URL.createObjectURL(file));
        }
    }
    return (
        <ListItem disablePadding >
            <ListItemButton onClick={() => handleOnClick()}>
                <ListItemAvatar>
                    <Avatar>
                        <FilePresent />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary={requirement} secondary={file?.name} />
            </ListItemButton>
            <ListItemSecondaryAction>
                <label htmlFor={`icon-button-file-${id}`} >
                    <Input accept="image/*" id={`icon-button-file-${id}`} type="file"
                        multiple={false} onChange={({ target }) => handleFile(target.files)}
                    />
                    <IconButton color="primary" aria-label="upload file" component="span" >
                        <FileUpload />
                    </IconButton>
                </label>
            </ListItemSecondaryAction>
        </ListItem>
    );
}

export default Requirements;