import { Box } from "@mui/material";
import ButtonAppBar from "./navbar1";


type LayoutProperties = {
    children: JSX.Element
}
export default function Layout({ children }: LayoutProperties) {
    return (
        <Box >
            <ButtonAppBar />
            <main>{children}</main>
        </Box>
    );
}