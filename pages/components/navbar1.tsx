import React, { useState } from "react";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import AppBar from "@mui/material/AppBar";
import Typography from "@mui/material/Typography";
import Badge from "@mui/material/Badge";
import Notifications from "@mui/icons-material/Notifications";
import AccountCircle from "@mui/icons-material/AccountCircle";
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { ChevronRight } from "@mui/icons-material";
import Menu from "@mui/icons-material/Menu"
import { Divider, List, ListItem, } from "@mui/material";
import Link from "next/link";

const navigationLinks = [
    { name: "Home", href: "/" },    
    { name: "Requeriment", href: "requirements" },
    { name: "CheckList", href: "checklist" },
    { name: "Expedient", href: "expedient" },
    { name: "Request", href: "requests" }
];

export default function Header() {
    const [openDrawer, setOpenDrawer] = useState(false);
    const [open, setOpen] = useState(false);

    return (
        <AppBar position="sticky">
            <Toolbar >
                <IconButton
                    onClick={() => setOpen(!open)}
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                >
                    <Menu />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    CertiDigital
                </Typography>
                <IconButton size="large" aria-label="show 17 new notifications" color="inherit">
                    <Badge badgeContent={17} color="error">
                        <Notifications />
                    </Badge>
                </IconButton>
                <IconButton size="large" color='inherit'>
                    <AccountCircle />
                </IconButton>
            </Toolbar>
            <SwipeableDrawer
                anchor="left"
                open={open}
                onOpen={() => setOpen(true)}
                onClose={() => setOpen(false)}
            >

                <div
                    onClick={() => setOpen(false)}
                    onKeyPress={() => setOpen(false)}
                    role="button"
                    tabIndex={0}
                >
                    <IconButton>
                        <ChevronRight />
                    </IconButton>
                </div>
                <Divider />
                <List>
                    {navigationLinks.map((item) => (
                        <ListItem key={item.name}>

                            <Link
                                href={item.href}
                            >
                                {item.name}
                            </Link>
                        </ListItem>
                    ))}
                </List>
            </SwipeableDrawer>

        </AppBar>
    );
}