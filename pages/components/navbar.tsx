import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Badge from '@mui/material/Badge';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Notifications from '@mui/icons-material/Notifications';
import { Container } from '@mui/material';

const navigationLinks = [
  { name: "Requeriments", href: "/requirements" },
  { name: "Projects", href: "#projects" },
  { name: "Resume", href: "/resume.pdf" },
];

export default function ButtonAppBar() {
  return (
      <AppBar position='sticky'>
        <Container maxWidth="md">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              CertiDigital
            </Typography>
            <IconButton size="large" aria-label="show 17 new notifications" color="inherit">
              <Badge badgeContent={17} color="error">
                <Notifications />
              </Badge>
            </IconButton>
            <IconButton size="large" color='inherit'>
              <AccountCircle />
            </IconButton>
          </Toolbar>
        </Container>
      </AppBar>
  );
}