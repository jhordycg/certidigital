type UserType = {
    id: string,
    name: string,
    lastName: string,
    dni: string,
    email: string,
    area: string,
    careers: string,
    institute: string,
    lastname: string,
    password: string,
    phone: string,
    rol: "COLABORATOR" | "COLABORATOR" | "STUDENT",
};

export default UserType;