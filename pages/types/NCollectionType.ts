type NCollectionType = {
    "entity-type": "document",
    type: "Collection",
    properties: {
        "dc:title": string,
        "collection:documentIds"?: string[],
    }
}

export default NCollectionType;