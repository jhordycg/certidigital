type AutoCollectionType = {
    params: {
        name: string,
        description?: string,
    }
}

export default AutoCollectionType;