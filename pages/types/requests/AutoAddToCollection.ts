type AddToCollectionType = {
    params: {
        collection: string,
    }
}

export default AddToCollectionType;