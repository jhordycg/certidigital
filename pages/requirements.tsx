import { Grid, Step, StepLabel, Stepper } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import Overview from "./components/requests/overview";
import Requirements from "./components/requirements";
import { useUser } from "./user-context";

function Request() {
    const { user } = useUser();
    const router = useRouter();

    useEffect(() => { user ?? router.push("/"); }, [user]);

    const [activeStep, setActiveStep] = useState(0);
    const [requirements, setRequirements] = useState<string[]>([]);

    const batchId = useRef<string>();

    const onNext = () => {
        console.log(batchId.current);
        if (activeStep < proccessFlow.length - 1) setActiveStep(activeStep + 1);
    }

    const onPrev = () => {
        console.log(activeStep - 1)
        if (activeStep >= 1) setActiveStep(activeStep - 1);
    }

    const attachRequirement = (fileId: string) => {
        console.log("file id: %s", fileId);
        setRequirements(requirements.concat([fileId]));
    }

    const proccessFlow = [
        // { label: "Pago de Trámite", },
        {
            label: "Antes de Empezar",
            component: <Overview onNext={onNext} onPrev={onPrev} batchId={batchId} />
        },
        {
            label: "Requisitos",
            component: <Requirements attachRequirement={attachRequirement} batchId={batchId} onNext={onNext} onPrev={onPrev} />
        },
        {
            label: "Validar información",
            component: <Overview onNext={onNext} onPrev={onPrev} batchId={batchId} />
        },
    ]

    return (
        <Grid container direction="column" paddingX={4} paddingY={3} height="90vh">
            <Grid item xs={1.5} >
                <Stepper activeStep={activeStep} >
                    {proccessFlow.map((item, key) => (
                        <Step key={key}>
                            <StepLabel color="default" >
                                {item.label}
                            </StepLabel>
                        </Step>
                    ))}
                </Stepper>
            </Grid>
            <Grid item xs>
                {proccessFlow[activeStep].component}
            </Grid>
        </Grid>
    )
}

export default Request;